package main

import "fmt"

func main() {
	//一个包中的变量或者方法名首字母小写只能在包内使用，首字母大写可以在包外被调用

	myBase()
}

func myBase() {
	var age int = 40
	// 与 && 第一个条件为false，后面的的条件不执行判断
	if age > 30 && age < 50 {
		fmt.Println("ok1")
	}
	if age >30 && age < 40 {
		fmt.Println("ok2")
	}
	// 或 || 第一个条件为true，后面的条件不执行判断
	if age > 30 || age < 50 {
		fmt.Println("ok3")
	}
	if age >30 || age < 40 {
		fmt.Println("ok4")
	}
	// 非 !
	if age > 30 {
		fmt.Println("ok5")
	}
	if !(age >30) {
		fmt.Println("ok6")
	}

}
