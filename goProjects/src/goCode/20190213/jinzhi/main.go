package main

import "fmt"

func main()  {
	//进制
	//二进制,0-1.将数字按照二进制输出
	var i int = 5
	fmt.Printf("%b \n",i) //101
	//八进制，0-7,以0开头
	var j int = 011
	fmt.Println(j) //9
	//十六进制，0-9以及A-F，以0x开头
	 var k int = 0x1A
	 fmt.Println(k) //26
}
