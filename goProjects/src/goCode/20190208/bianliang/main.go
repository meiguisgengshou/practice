package main

import (
	"fmt"
	"strconv"
)

func main() {
	//createVar()
	//sToBool()
	//strToInt()
	strToFloat()
}
//变量申明
func createVar()  {
	//变量声明之后未赋值，则为默认值，int=》0
	var i int
	fmt.Println("i=", i)
	//使用:=可以省略var 进行声明赋值
	name := "tom"
	fmt.Println("name:",name)
	//多变能量声明
	var n1,n2,n3 int
	fmt.Println("n1=",n1,",n2=",n2,",n3=",n3)
	var n4,n5,n6=23,"jiujiu",34.3
	fmt.Println("n4=",n4,",n5=",n5,",n6=",n6)

	n7,n8:=34,"kjjk"
	fmt.Println("n7=",n7,",n8=",n8)
}
//字符串转bool，字符串无法转bool，返回默认值，即false
func sToBool()  {
	var str = "true"
	var b bool
	b,_ = strconv.ParseBool(str)
	fmt.Printf("b type %T,value %v",b,b)
}
//字符串转数字,若字符串不能转数字，返回默认值，即0
func strToInt()  {
	var str = "12540"
	var num int64
	num,_=strconv.ParseInt(str,10,64)
	fmt.Printf("num type %T,value %v",num,num)

}
func strToFloat()  {
	var str = "125.32"
	var f float64
	f,_ = strconv.ParseFloat(str,64)
	fmt.Printf("f type %T,value %v",f,f)
}
