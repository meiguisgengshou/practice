package main

import "fmt"

func main()  {
	myBase()
}

func myBase()  {
	var i int = 10
	fmt.Println("i地址为",&i)
	/*指针变量,
	ptr为指针变量，
	类型为*int，
	值为整数变量i的地址，
	*ptr 标识ptr指向的值，即i的值
	改变*ptr的值，i的值也会跟着改变
	*/
	var ptr *int = &i
	fmt.Printf("ptr type %T,value %v\n",ptr,ptr)
	fmt.Println("ptr 地址为",&ptr)
	fmt.Println("*ptr 的值为",*ptr)
}
