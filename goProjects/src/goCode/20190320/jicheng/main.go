package main

import "fmt"

func main()  {
	test01()
}
func test01()  {
	p1 := &Primary{}
	//设置属性与调用方法
	p1.Student.Name = "tom"
	p1.Student.Age = 8
	p1.testing()
	p1.Student.SetScore(89)
	p1.Student.showInfo()

	//另一种简化写法
	c1 := &Colllege{}
	c1.Name = "john"
	c1.Age = 21
	c1.testing()
	c1.SetScore(78)
	c1.showInfo()
}

func test2()  {
	//嵌套有名结构体时，访问属性需要带上有名结构体的名字
	type A struct {
		Name string
		Age int
	}
	type B struct {
		a A
	}
	var b B
	b.a.Age = 3
	//结构体的匿名字段是基本类型，但不能重复
	type C struct {
		A
		int
	}
	var c C
	c.int = 9
	fmt.Println(c)
	//多重继承：如果一个struct里面嵌套了多个匿名结构体，就实现了多重继承
}

//学生
type Student struct {
	Name string
	Age int
	Score int
}
//显示学生信息
func (stu *Student)showInfo()  {
	fmt.Printf("name=>%v,age=>%d,score=>%d\n",stu.Name,stu.Age,stu.Score)
}
//设置分数
func (stu *Student)SetScore(score int)  {
	stu.Score = score
}

// 小学生
type Primary struct {
	Student //设置匿名结构体，相当于继承，可使用结构体的属性与方法（包括私有与共有）
}

func (pri *Primary)testing()  {
	fmt.Println("primary student is testing...")
}
//  大学生
type Colllege struct {
	Student //设置匿名结构体，相当于继承，可使用结构体的属性与方法
}

func (col *Colllege)testing()  {
	fmt.Println("college student is testing...")
}



