package main

import (
	"fmt"
	"goCode/20190320/factory/model"
)

func main()  {
	//test1()
	test2()
}

func test1()  {
	var s1 = model.Stu{Name:"lucky",Score:23.2,}
	fmt.Println("s1=>",s1)
}
//利用工厂模式可以挎包获取私有的结构体与其属性
func test2()  {
	p1 := model.NewPersion("dudu")
	fmt.Println("p1=>",*p1)
	fmt.Println("name=",p1.GetPersionName())
}
