package main

import "fmt"

//接口中声明方法
type Usb interface {
	start()
	stop()
}

type Phone struct {

}
//结构体中实现方法
func (p Phone)start()  {
	fmt.Println("phone start working...")
}

func (p Phone)stop()  {
	fmt.Println("phone stop working...")
}
type Camera struct {

}
func (c Camera)start()  {
	fmt.Println("camera start working...")
}

func (c Camera)stop()  {
	fmt.Println("camera stop working...")
}

type Computer struct {

}
//实现usb接口就是实现了usb中声明的所有方法
func (c Computer)Working(usb Usb)  {
	usb.start()
}

func main()  {
	test01()
}

func test01()  {
	camera := Camera{}
	phone := Phone{}
	computer := Computer{}
	computer.Working(camera)
	computer.Working(phone)
}

