package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	//suijishu()
	//label()
	mySum()
}

//生成随机数，99停止
func suijishu() {
	num := 0
	for {
		// 设置随机数种子，不然每次返回相同的数，这里用纳秒
		rand.Seed(time.Now().UnixNano())
		randNum := rand.Intn(100) + 1
		fmt.Println(randNum)
		num ++
		if randNum == 99 {
			break
		}
	}
	fmt.Printf("总共运行了%d次", num)
}

func label() {
	//break默认跳出就近循环
	for i := 0; i < 4; i++ {
		for j := 0; j < 6; j++ {
			if j == 2 {
				break
			}
			fmt.Println("j=", j)
		}
	}
	fmt.Println("=================")
	//break配合标签使用，跳出对应的标签循环
label1:
	for i := 0; i < 4; i++ {
		for j := 0; j < 6; j++ {
			if j == 2 {
				break label1
			}
			fmt.Println("j=", j)
		}
	}
}

func mySum()  {
	sum := 0
	for i:=1;i<=100;i++{
		sum +=i
		if sum >20{
			fmt.Println("第一个数为",i)
			break
		}
	}
}
