package main

import "fmt"

func main() {
	//forDemo()
	//forrange()
	//demo1()
	//demo2()
	//tar1()
	//tar2()
	jiu()
}
func forDemo() {
	for i := 1; i <= 10; i++ {
		fmt.Println("just do for",i)
	}

	for i:=1;i<3;i++{
		fmt.Println("second for ",i)
	}
	//遍历字符串，如果字符串中有中文，需要将字符串转换成[]rune类型，否则乱码
	var str string = "hello 小九"
	str2 := []rune(str)
	for i :=0; i<len(str2); i++ {
		fmt.Printf("%c \n",str2[i])
	}
}
func forrange()  {
	//str中有汉字，无需将str转换成其他类型，但是汉字的index占3位
	var str string = "hello,world 世界"
	for index,val:=range str{
		fmt.Printf("index=%d,val=%c \n",index,val)
	}
}

func demo1()  {
	var num int = 0
	var numSum int = 0
	for i:=1;i<=100;i++{
		if i%9 == 0{
			num ++
			numSum += i
		}
	}
	fmt.Printf("100以内9的倍数有%d个，其和为%d \n",num,numSum)
}

func demo2()  {
	var total int =6
	for i:=0; i<=total; i++ {
		fmt.Printf("%d + %d = %d\n",i,total-i,total)
	}
}
//实心金字塔
func tar1()  {
	var n int = 9
	for i:=1;i<=n;i++{
		//空格
		for j:=1;j<=n-i;j++{
			fmt.Print(" ")
		}
		//*
		for k:=1;k<=2*i-1;k++{
			fmt.Print("*")
		}
		fmt.Println()
	}
}
//空心金字塔
func tar2()  {
	var n int = 9
	for i:=1;i<=n;i++{
		//空格
		for j:=1;j<=n-i;j++{
			fmt.Print(" ")
		}
		//*
		for k:=1;k<=2*i-1;k++{
			if (k == 1) || (k == (2*i-1)) || (i == n){
				fmt.Print("*")
			}else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
}
//九九乘法表
func jiu()  {
	var n int = 9
	for i:=1;i<=n;i++{
		for j:=1;j<=i;j++{
			fmt.Printf("%d * %d = %d\t",j,i,i*j)
		}
		fmt.Println()
	}
}

/*func breakDemo()  {
	var times int = 0
	while true {
		times ++

	}
}*/
