package main

import "fmt"

//struct是值拷贝，不是引用类型
type Person struct {
	Name string
	Age int
	Sex string
}

func main()  {
	test1()
}

func changePerson(person Person)  {
	person.Sex = "female"
}

func test1()  {
	var p1 Person
	p1.Name = "tom"
	p1.Age = 18
	p1.Sex = "male"
	fmt.Println(p1)
	changePerson(p1)
	fmt.Println(p1)

	var p2 Person = Person{}
	p2.Name = "jay"
	p2.Sex = "male"
	fmt.Println(p2)
	//此时go的设计者为了照顾程序员的使用习惯，(*p3).Name = "nmae" 等价于 p3.Name="name"
	var p3 *Person = new(Person)
	(*p3).Name = "jhon"
	p3.Sex = "female"
	p3.Age = 25
	fmt.Println(*p3)

	var p4 *Person = &Person{}
	(*p4).Name = "Mary"
	p4.Sex = "female"
	p4.Age =30
	fmt.Println(*p4)

}
