package main

import "fmt"

type Persion struct {
	Name string
	Age int
}

func main() {
	test()
	test2()

}

// 方法，用于操作指定类型数据,且只有指定类型的数据能调用该方法
func (persion Persion)getName()  {
	println(persion.Name)
}

func test()  {
	var p1 Persion
	p1.Name="Tom"
	p1.Age = 18
	p1.getName()

}
//struct的初始赋值方式
func test2(){
	//按照属性的先后顺序赋值
	var p1  =Persion{"jay",15}
	fmt.Println("p1",p1)
	//按照属性赋值
	p2 := Persion{
		Name:"tony",
		Age:12,
	}
	fmt.Println("p2=",p2)
	//指针类型
	var p3 *Persion = &Persion{"jhon",18,}
	fmt.Println("p3=",*p3)
	p4 := &Persion{
		Name:"lucy",
		Age:25,
	}
	fmt.Println("p4=",*p4)
}
