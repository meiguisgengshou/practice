package main

import (
	"fmt"
	"strings"
)
/*
1.函数中的变量是局部的，函数外不生效
2.基本数据类型与数组默认是值传递，函数内改变不影响原来的值
3.如果需要在函数内修改影响到原来的值（基本数据类型与数组），可以通过传入变量地址&，在函数内以指针方式操作变量
4.go中不支持函数重载
 */
func main() {
	demo1(4)
	demo2(4)
	//fmt.Println("n=>",hanshu(2))
	//fmt.Println("n=>",peach(10))
	//a := 10
	//b := 20
	//swap(&a, &b)
	//fmt.Printf("a=%v,b=%v\n", a, b)

	//f := makeSuffix(".jpg")
	//fmt.Println(f("winter"))
	//fmt.Println(f("spring.jpg"))
}

//返回多个值
func getRes(num1 int, num2 int) (int, int) {
	return num1 + num2, num1 - num2
}

//变量值交换
func swap(n1 *int, n2 *int) {
	tmp := *n1
	*n1 = *n2
	*n2 = tmp
}

//递归
func demo1(n int) {
	if n > 2 {
		n--
		demo1(n)
	}
	fmt.Println("n = ", n)
}
func demo2(n int) {
	if n > 2 {
		n--
		demo2(n)
	} else {
		fmt.Println("n = ", n)
	}
}

/*斐波那契数列*/
func fib(n int) int {
	if n == 1 || n == 2 {
		return 1
	} else {
		return fib(n-1) + fib(n-2)
	}
}
func hanshu(n int) int {
	if n == 1 {
		return 3
	}
	return 2*hanshu(n-1) + 1
}

/*
有一堆桃子，第一天吃了一半外加一个，第二天再吃剩余的一半外加一个，以此类推，第n天后（还没吃）剩下1个，求总共有多少桃子
*/
func peach(n int) int {
	if n == 1 {
		return 1
	}
	return (peach(n-1) + 1) * 2
}

//闭包函数，判断后缀
func makeSuffix(suffix string) func(string) string {
	return func(name string) string {
		if !strings.HasSuffix(name, suffix) {
			return name + suffix
		}
		return name
	}
}
