package main

import "fmt"

func main()  {
	test()
	fmt.Println("main的后续")
}

func test()  {
	defer func() {
		//获取错误信息,此时程序不会崩溃，会继续向下执行
		err := recover()
		if err != nil{
			fmt.Println("捕获错误：",err)
		}
	}()
	num1:=10
	num2 :=0
	res := num1/num2
	fmt.Println("res = ",res)
}
