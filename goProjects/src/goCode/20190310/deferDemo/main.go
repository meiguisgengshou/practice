package main

import "fmt"

func main() {
	res := sum(10, 20)
	fmt.Println("res = ", res)
}

func sum(n1, n2 int) int {
	//defer 延迟机制，defer里面的代码会在函数执行完之后执行，并且顺序是先进后出
	defer fmt.Println("n1 = ", n1)
	defer fmt.Println("n2 = ", n2)
	n1++
	res := n1 + n2
	fmt.Println("my res = ", res)
	return res
}
