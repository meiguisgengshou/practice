package main

import "fmt"

func main()  {
	//test()
	test2()
}
func test()  {
	var intArr = [5]int{11,22,33,44,55}
	var slice = intArr[1:4]
	fmt.Println("array:",intArr)
	fmt.Println("slice:",slice)
	fmt.Println("slice length:",len(slice))
	fmt.Println("slice cap",cap(slice))//容量，动态变化
}

func test2()  {
	//切片的三种创建方式
	var intArr = [5]int{1,2,3,4,5}
	var slice1 = intArr[1:3]
	fmt.Println("alice1=",slice1)

	var slice2 []int = make([]int,2,5)
	slice2[1] = 4399
	fmt.Printf("slice2=%v len=%d cap=%d\n",slice2,len(slice2),cap(slice2))

	var slice3 []string = []string{"tom","jerrey"}
	fmt.Printf("slice3=%v len=%d cap=%d\n",slice3,len(slice3),cap(slice3))
}
