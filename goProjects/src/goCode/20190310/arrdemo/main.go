package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main()  {
	test2()
	test3()
	test4()
}
func test1()  {
	//数组的初始化
	var arr1 [3]int = [3]int{1,2,3}
	fmt.Println("arr1:",arr1)
	var arr2 = [3]int{4,5,6}
	fmt.Println("arr2:",arr2)
	var arr3 = [...]int{7,8,9}
	fmt.Println("arr3:",arr3)
	var arr4 = [...]int{1:10,0:15,2:16}
	fmt.Println("arr4:",arr4)
}
func test2()  {
	//数组遍历
	var names = [...]string{"tom","jay","cake"}
	for index,value := range names{
		fmt.Printf("name[%d]=%v\n",index,value)
	}
}
func test3()  {
	var chars [26]byte
	for i:=0;i<26 ;i++  {
		chars[i] = 'A'+byte(i)
	}
	for i,v := range chars {
		fmt.Printf("%d=>%c\t",i,v)
	}
	fmt.Println()
}
func test4()  {
	//生成水机数组，然后反转打印数组
	var intArr [5]int
	rand.Seed(time.Now().UnixNano())
	var len int = len(intArr)
	for i:=0;i<len;i++{
		intArr[i] = rand.Intn(999)
	}
	fmt.Println(intArr)
	tmp := 0
	for i:=0;i<len/2;i++{
		tmp = intArr[i]
		intArr[i] = intArr[len-1-i]
		intArr[len-1-i] = tmp
	}
	fmt.Println(intArr)

}
