package main

import "fmt"

func main() {
	intArr := [5]int{15, 2, 47, 41, 69}
	fmt.Println("before:", intArr)
	bubble(intArr[:])
	fmt.Println("after:", intArr)
	fmt.Println(binaryFind(intArr[:], 0, len(intArr[:])-1, -5))
}

//冒泡排序
func bubble(slice []int) {
	tmp := 0
	flag := true
	for i := 0; i < len(slice)-1; i++ {
		for j := 0; j < len(slice)-1-i; j++ {
			if slice[j] > slice[j+1] {
				flag = false
				tmp = slice[j]
				slice[j] = slice[j+1]
				slice[j+1] = tmp
			}
		}
		if flag == true {
			break
		} else {
			flag = true
		}
	}
}

//二分查找
func binaryFind(slice []int, leftIndex int, rightIndex int, value int) int {
	if leftIndex > rightIndex {
		return -1
	}
	middleIndex := (leftIndex + rightIndex) / 2
	if value < slice[middleIndex] {
		return binaryFind(slice, leftIndex, middleIndex-1, value)
	} else if value > slice[middleIndex] {
		return binaryFind(slice, rightIndex+1, rightIndex, value)
	} else {
		return middleIndex
	}
}
