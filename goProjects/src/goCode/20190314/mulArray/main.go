package main

import "fmt"

func main() {
	test1()
	test2()
}

func test1() {
	var arr [4][5]int
	arr[1][3] = 5
	arr[2][1] = 1
	arr[3][4] = 7
	for i := 0; i < 4; i++ {
		for j := 0; j < 5; j++ {
			fmt.Print(arr[i][j], " ")
		}
		fmt.Println()
	}
}

func test2() {
	//二维数组的遍历
	var arr = [2][3]int{{1, 2, 3}, {4, 5, 6}}
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr[i]); j++ {
			fmt.Printf("%d\t", arr[i][j])
		}
		fmt.Println()
	}

	for i, v := range arr {
		for j, v2 := range v {
			fmt.Printf("arr[%d][%d]=%d\t", i, j, v2)
		}
		fmt.Println()
	}
}
